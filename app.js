const express = require("express");
const app = express();



const userRouter = require("./routes/userRoutes")
app.use(express.json())
app.use("/api/v1/users", userRouter)
/*  Starting the server on port 4001. */
const port = 4000;
app.listen(port, () => {
  console.log(`App running on port ${port} ..`);
});
module.exports = app